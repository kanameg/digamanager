#!/usr/bin/env ruby
# coding: utf-8

require 'net/http'
require 'kconv'
require 'digest/md5'
require 'base64'
require 'uri'
require 'nokogiri'
require 'yaml'
require 'logger'

# 
# DIGA Manager操作クラス
#
class DigaManager
  
  # USER AGENT
  USER_AGENT = 'Ruby/DigaManager'
  
  # 設定ファイル
  DEFAULT_CONFIG_FILE = 'config.yaml'
  
  # ログファイル名
  DEFAULT_LOG_FILE = 'digamanager.log'

  # イニシャライザ
  # 
  def initialize
    @log = Logger.new(DEFAULT_LOG_FILE, 11)
    @config = YAML.load(open(DEFAULT_CONFIG_FILE.to_s).read)
    @ipaddress = @config['ipaddress']
    @password = @config['password']
    
    @log.debug(@config)
  end

  # 地デジ放送局一覧
  # @return [Hash] 放送局一覧 {'チャンネル番号' => '放送局名'}
  def dtv_stations
    @config['broadcast']['dtv']['stations']   # 地上Dの放送局一覧
  end

  # BS放送局一覧
  # @return [Hash] 放送局一覧 {'チャンネル番号' => '放送局名'}
  def bs_stations
    @config['broadcast']['bs']['stations']    # BSの放送局一覧
  end

  # CS1放送局一覧
  # @return [Hash] 放送局一覧 {'チャンネル番号' => '放送局名'}
  def cs1_stations
    @config['broadcast']['cs1']['stations']   # CS1の放送局一覧
  end

  # CS2放送局一覧
  # @return [Hash] 放送局一覧 {'チャンネル番号' => '放送局名'}
  def cs2_stations
    @config['broadcast']['cs2']['stations']   # CS1の放送局一覧
  end

  # 放送局一覧
  # @return [Hash] 放送局一覧 {'チャンネル番号' => '放送局名'}
  def stations
    {}.merge(dtv_stations).merge(bs_stations).merge(cs1_stations).merge(cs2_stations)
  end

  # 録画圧縮モード一覧
  # @return [Hash] 録画モード一覧 {'コード' => 'モード名'}
  def modes
    @config['mode']   # 録画モード
  end
  
  # 放送一覧
  # @return [Hash] 放送一覧 {'コード' => '放送名(地上D,BS,CS1,CS2)'}
  def tunners
    @config['broadcast'].inject({}){|tunners, (key, broadcast)|
      tunners[broadcast['code']] = broadcast['type']
      tunners
    }   # 放送一覧
  end

  
  # DIGA Managerにログイン
  # 
  def login
    Net::HTTP.start(@ipaddress, 80) do |http|
      http.get('/', "User-Agent" => USER_AGENT)
      res = http.post('/cgi-bin/prevLogin.cgi', '').body.toutf8
      
      # 認証用ダイジェストの生成
      nonce = res.match(/var nonce="([0-9]+)";/)[1]
      digest = URI.encode_www_form_component(Base64.encode64(Digest::MD5.digest(nonce + @password)).chop)
      query = "passwd=&nonce=" + nonce + "&digest=" + digest;
      
      res = http.post('/cgi-bin/loginPsWd.cgi', query).body.toutf8
      res = http.post('/cgi-bin/topMenu.cgi', query).body.toutf8
    end

    
    # 録画予約実行
    #
    # @param  param [Hash] param  webのFORMから取得したパラメータをそのまま使用
    # @return [True/False, String]   予約成功/失敗, 表示文字列
    def submit_reserve(params)
      entry = {
	'cRVMD'	 => params['begin_date'].gsub(/-/, '')[4..-1],
	'cRVHM1' => params['begin_time'].gsub(/:/, ''),
	'cRVHM2' => params['end_time'].gsub(/:/, ''),
	'cCHSRC' => params['tunner'],
	'cCHNUM' => params['ch'],
	'cRSPD1' => params['mode'],
	'cTHEX'	 => params['title'],
      }
      entry.map {|q| @log.debug(q.join(":"))}
      
      Net::HTTP.start(@ipaddress, 80) do |http|
        query =  'RSV_FIX=&cRPG=1011&cRHEX=&cTSTR=&cRHEXEX=&' + entry.map{|e| e.join('=')}.join('&').tosjis
        res = http.post('/cgi-bin/reserve_add.cgi', query).body.toutf8
        
        # "予約内容に間違いがあります"
        # "間違ったチャンネルが選択されました"
        error = res.scan(/<font color="#FF0000">\n(.+)<br><\/font>/)
        return false, error[0][0] unless error.empty?
        
        confirm = res.scan(/<input type="hidden" name="(\w*)" value="([\w\.\/\-_]*)">/)
        query = 'RSV_EXEC=&cRPG=1011&' + confirm.map{|c| c.join('=')}.join('&')
        res = http.post('/cgi-bin/reserve_addq.cgi', query).body.toutf8
        
        # "予約が重複しています"
        # "予約が設定できませんでした"
        # "指定した日付の予約はできません"
        error = res.scan(/<font color="#FF0000">\n(.+)<br><\/font>/)
        return false, error[0][0] unless error.empty?
      end

      # 予約成功
      return true, "予約が完了しました"
    end

    
    # DIGAの状態を取得
    #
    def fetch_status
      Net::HTTP.start(@ipaddress, 80) do |http|
        res = http.post('/cgi-bin/dvdr/dvdr_ctrl.cgi', 'RSV_FIX=&cRPG=1011&cRHEX=&cTSTR=&cRHEXEX=').body.toutf8
        #print "#{res}"
      end
    end
    
    
    # DIGAの録画予約一覧を取得
    # ログイン済の必要あり
    # 
    # @return [Array] 録画予約一覧
    def fetch_reserve_list
      Net::HTTP.start(@ipaddress, 80) do |http|  
        res = http.post('/cgi-bin/reserve_list.cgi', 'cRPG=&cRHEX=&cTSTR=&cRHEXEX=&cCMD_REVIEW.x=&cCMD_REVIEW.y=').body.toutf8
        #print "#{res}"
        parse_doc = Nokogiri::HTML.parse(res)
        reserve_table = parse_doc.xpath('/html/body/form/table[2]/tr[2]/td/table/tr')[1..-1]
        reserve_list = reserve_table.map {|reserve_tr|
          parse_reserve(reserve_tr)
        }
      end
    end
    

    # 録画予約情報をパース
    # 録画予約テーブル情報 no:予約番号, date:日付, weekday:曜日, tunner:放送, ch:チャンネル(放送局名)
    #                      begin_time:開始時間, end_time:終了時間, mode:録画モード(HX,..)
    #                      media:録画メディア(HDD, BD,..), note:備考
    #
    # @param  [Nokogiri] reserve_tr 予約テーブル(tr)
    # @return [Hash] 予約情報 [no, data, weekday, tunner, ch, begin_time, end_time, mode, media, [note, ..]]
    def parse_reserve(reserve_tr)
      # 録画リンク
      path, query = get_reserve_list_link(reserve_tr) # 録画link path, query
      
      # 録画情報
      no = get_reserve_list_no(reserve_tr)            # 予約番号
      date = get_reserve_list_date(reserve_tr)        # 録画日
      weekday = get_reserve_list_weekday(reserve_tr)  # 録画曜日
      tunner = get_reserve_list_tunner(reserve_tr)    # 放送
      ch = self.stations[get_reserve_list_ch(reserve_tr)]            # チャンネル 3桁 -> 放送局名
      
      begin_time = get_reserve_list_start(reserve_tr) # 開始時間
      end_time = get_reserve_list_end(reserve_tr)     # 終了時間
      mode = get_reserve_list_mode(reserve_tr)        # 録画モード
      media = get_reserve_list_media(reserve_tr)      # 録画メディア
      notes = get_reserve_list_note(reserve_tr)       # 備考
      
      {'no' => no, 'date' => date, 'weekday' => weekday, 'tunner' => tunner, 'ch' => ch,
       'begin_time' => begin_time, 'end_time' => end_time, 'mode' => mode,
       'media' => media, 'note' => notes, 'path' => path, 'query' => query}
    end

    
    # 予約詳細へのリンク(path, query)を取得
    # @param  [Nokogiri] tr 予約行
    # @return [Array]       予約へのlink
    # @return [String]      予約 path
    # @return [String]      予約 query
    def get_reserve_list_link(tr)
      link = tr.xpath('td[1]/a').attribute('href').value
      path, query = link.split('?')
    end
    
    # 予約番号を取得
    # @param  [Nokogiri] tr 予約行
    # @return [Integer]     予約番号 1〜
    def get_reserve_list_no(tr)
      tr.xpath('td[1]').text.to_i
    end

    # 予約日を取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      予約日 MM/DD
    def get_reserve_list_date(tr)
      tr.xpath('td[3]/div').text.match(/([0-9]+\/[0-9]+)\(.+\)/)[1]
    end

    # 予約曜日を取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      予約曜日 漢字
    def get_reserve_list_weekday(tr)
      tr.xpath('td[3]/div').text.match(/[0-9]+\/[0-9]+\((.+)\)/)[1]
    end

    # 予約放送を取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      放送 (地上D, BS etc)
    def get_reserve_list_tunner(tr)
      tr.xpath('td[4]').text.split(' ')[1]
    end

    # 予約チャンネルを取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      番号3桁
    def get_reserve_list_ch(tr)
      tr.xpath('td[4]').text.split(' ')[2]
    end
    
    # 予約開始時間を取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      時間 HH:MM形式
    def get_reserve_list_start(tr)
      tr.xpath('td[5]/div').text.match(/([0-9]+:[0-9]+)〜[0-9]+:[0-9]+/)[1]
    end

    # 予約終了時間を取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      時間 HH:MM形式
    def get_reserve_list_end(tr)
      tr.xpath('td[5]/div').text.match(/[0-9]+:[0-9]+〜([0-9]+:[0-9]+)/)[1]
    end

    # 予約メディアを取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      メディア HDD BD
    def get_reserve_list_media(tr)
      tr.xpath('td[6]/div').text.match(/.+\((.+)\)/)[1]
    end

    # 予約録画モードを取得
    # @param  [Nokogiri] tr 予約行
    # @return [String]      録画モード
    def get_reserve_list_mode(tr)
      tr.xpath('td[6]/div').text.match(/(.+)\(.+\)/)[1]
    end

    # 予約録画備考を取得
    # @param  [Nokogiri] tr 予約行
    # @return [Array]       備考一覧
    def get_reserve_list_note(tr)
      tr.xpath('td[7]').text.gsub(' ', '').split('、')
    end

    
    # DIGAの録画予約の詳細を取得
    #  指定した番号の予約がない場合はnilを返す
    # @param  [Integer] no  予約番号
    # @param  [String] dummy ダミーデータ
    # @return [String] 結果文字列
    def fetch_reserve(no, dummy)
      Net::HTTP.start(@ipaddress, 80) do |http|  
        
        path = '/cgi-bin/reserve_list.cgi'
        query = "ANC_RSVLSTNO=#{no}&cDMYDT=#{dummy}"
        res = http.post(path, query).body.toutf8
        #print "#{res}"
        parse_doc = Nokogiri::HTML.parse(res)
      
        # 情報取得
        title        = get_reserve_title(parse_doc)
        date         = get_reserve_date(parse_doc)
        begin_time   = get_reserve_start_time(parse_doc)
        end_time     = get_reserve_end_time(parse_doc)
        tunner       = get_reserve_tunner(parse_doc)
        ch           = self.stations[get_reserve_ch(parse_doc)]
        mode         = get_reserve_mode(parse_doc)
        media        = get_reserve_media(parse_doc)
        #media_remain = get_reserve_media_remain(parse_doc)
        
        {'title' => title, 'date' => date, 'begin_time' => begin_time, 'end_time' => end_time,
         'tunner' => tunner, 'ch' => ch, 'mode' => mode, 'media' => media}
      end
    end

    # タイトル取得
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String] タイトル文字列
    def get_reserve_title(doc)
      doc.xpath('/html/body/form/table[3]/tr/td/table/tr[7]/td/table/tr/td[2]/input')[0].attributes['value'].value
    end

    # 録画日取得
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String] MM/DD形式日時
    def get_reserve_date(doc)
      m = doc.xpath('/html/body/form/table[3]/tr/td/table/tr[3]/td/table/tr/td[2]/input[1]')[0].attributes['value'].value
      month, day = m.match(/([0-9]{2})([0-9]{2})/)[1..-1]
      month + "/" + day
    end


    # 開始時刻取得
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String] HH:MM形式時刻
    def get_reserve_start_time(doc)
      m = doc.xpath('/html/body/form/table[3]/tr/td/table/tr[4]/td/table/tr/td[2]/input[1]')[0].attributes['value'].value
      hour, minute = m.match(/([0-9]{2})([0-9]{2})/)[1..-1]
      hour + ":" + minute
    end

    # 終了時刻取得
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String] HH:MM形式時刻
    def get_reserve_end_time(doc)
      m = doc.xpath('/html/body/form/table[3]/tr/td/table/tr[4]/td/table/tr/td[4]/input[1]')[0].attributes['value'].value
      hour, minute = m.match(/([0-9]{2})([0-9]{2})/)[1..-1]
      hour + ":" + minute
    end

    # 録画放送
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String]  放送名
    def get_reserve_tunner(doc)
      m = doc.xpath('/html/body/form/table[3]/tr/td/table/tr[5]/td/table/tr/td[2]').text
      m.split(' ')[0].gsub(/\n/, '')
      #m = doc.xpath('/html/body/form/table[2]/tr/td/div/table/tr[2]/td[3]/div/font/b').text
      #m.split(' ')[0]
    end

    # 録画チャネル
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String]  チャネル番号
    def get_reserve_ch(doc)
      m = doc.xpath('/html/body/form/table[3]/tr/td/table/tr[5]/td/table/tr/td[2]').text
      m.split(' ')[1].gsub(/\n/, '')
      #m = doc.xpath('/html/body/form/table[2]/tr/td/div/table/tr[2]/td[3]/div/font/b').text
      #m.split(' ')[1]
    end

    # 録画モード
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String]  録画モード
    def get_reserve_mode(doc)
      doc.xpath('/html/body/form/table[2]/tr/td/div/table/tr[2]/td[4]/div/font/b').text
    end

    # 録画メディア
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String]  録画メディア
    def get_reserve_media(doc)
      doc.xpath('/html/body/form/table[2]/tr/td/div/table/tr[2]/td[1]/div/font/b').text
    end

    # 録画メディア残量
    # @param  [Nokogiri] doc  htmlドキュメント
    # @return [String]  録画メディア残量
    def get_reserve_media_remain(doc)
      doc.xpath('/html/body/form/table[2]/tr/td/div/table/tr[2]/td[5]/div/font/b').text
    end
    
  end
  
end
