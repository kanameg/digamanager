## DigaManager ##

DIGAのホスト名が**diga.hostname.com**の場合

* 予約一覧
** 予約パス
```
http://diga.hostname.com/cgi-bin/reserve_list.cgi
```
** オプション
```
RSV_FIX : 空
TG      : /cgi-bin/reserve_add.cgi
SID     : 空
cRVMD   : 日付 MMDD
cRVHM1  : 開始時間 HHMM
cRVHM2  : 終了時間 HHMM
cCHSRC  : 放送 [e, 4, 6, 7, 3]
cCHNUM  : チャンネル番号
cRSPD1  : 録画モード 数字 
cTHEX   : タイトル文字列
```

* 予約
```
http://diga.hostname.com/cgi-bin/reserve_add.cgi
```
* 予約削除
```
```